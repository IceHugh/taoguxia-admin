const env = process.env.NODE_ENV;
const baseUrl = {
  'development': 'http://120.79.240.209:80',
  // 'development': 'http://172.20.10.4:8001',
  // 'production': 'http://120.79.240.209:80',
  'production': 'https://www.taoguxia.com',
};

export default {
  baseUrl: baseUrl[env],
};
