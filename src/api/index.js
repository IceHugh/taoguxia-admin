import Axios from '../utils/axios';

const Api = {
  login(data) {
    return Axios.post('/login', data);
  },
  getAuditList(data) {
    return Axios.post('/backstage/project/auditlist', data);
  },
  getAuditDetail(data) {
    return Axios.get('/backstage/project/audit/detail', data);
  },
  operateProject(data) {
    return Axios.post('/backstage/project/audit', data);
  },
  get(data) {
    return Axios.post('/backstage/project/audit', data);
  },
  getFinishedList(data) {
    return Axios.post('/backstage/project/finishedlist', data);
  },
  finishProject(data) {
    return Axios.post('/backstage/project/finish', data);
  },
  getSettleList(data) {
    return Axios.post('/backstage/project/settlelist', data);
  },
  setSettleDetail(data) {
    return Axios.post('/backstage/project/settledetail', data);
  },
  finishSettle(data) {
    return Axios.post('/backstage/project/settlefinish', data);
  },
  updateStatis(data) {
    return Axios.post('/backstage/statis/data/update', data);
  },
  repaymentCollect(data) {
    return Axios.post('/backstage/project/repaymentcollect', data);
  },
};
export default Api;
