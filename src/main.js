// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import 'resetcss.css';

import App from './App'
import router from './router'
import store from './store';

import './styles/layout.styl';
Vue.config.productionTip = false

import iView from 'iview';
import 'iview/dist/styles/iview.css';
Vue.use(iView);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
