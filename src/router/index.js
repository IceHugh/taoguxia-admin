import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/views/index';
import Login from '@/views/login';
import Audit from '@/views/audit';
import Finished from '@/views/finished';
import Clearing from '@/views/clearing';

import AuditDetail from '@/views/auditdetail';
import FinishedDetail from '@/views/finishedDetail';
import ClearingDetail from '@/views/clearingDetail';
import Suspension from '@/views/suspension';
import Config from '@/views/config';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: 'login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/index',
      // name: 'index',
      component: Index,
      children: [
        {
          path: '',
          redirect: 'audit'
        },
        {
          path: 'audit',
          name: 'audit',
          component: Audit,
        },
        {
          path: 'audit/detail/:id/:type',
          name: 'auditdetail',
          component: AuditDetail,
          props: true,
        },
        {
          path: 'finished',
          name: 'finished',
          component: Finished,
        },
        {
          path: 'finished/detail/:id',
          name: 'finisheddetail',
          component: FinishedDetail,
          props: true,
        },
        {
          path: 'clearing',
          name: 'clearing',
          component: Clearing,
        },
        {
          path: 'clearing/detail/:id/:amount/:type',
          name: 'clearingdetail',
          component: ClearingDetail,
          props: true,
        },
        {
          path: 'clearing/suspension/:id',
          name: 'suspension',
          component: Suspension,
          props: true,
        },
        {
          path: 'config',
          name: 'config',
          component: Config,
        },
      ]
    }
  ]
})
